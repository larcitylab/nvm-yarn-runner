FROM node:lts

###### IMPORTANT! Do not change the home directory ###########
ENV HOME /home/node
ENV STAGE_DIR /opt/stage

# Designate the directory for NVM
ENV NVM_DIR "${HOME}/.nvm"
ENV PATH "${NVM_DIR}:$PATH"

# Coerce the home directory
RUN mkdir -vp ${HOME}

RUN echo "Path: ${PATH}"

# Will also propagate $STAGE_DIR
RUN mkdir -p ${NVM_DIR}

WORKDIR ${STAGE_DIR}

# Prep environment
RUN apt update 
RUN apt install -y nano tree git apt-transport-https

# setup heroku
RUN curl https://cli-assets.heroku.com/install.sh | sh
RUN heroku --version

# setup yarn
RUN git clone https://gitlab.com/larcitylab/shared-scripts.git && cd ${STAGE_DIR}/shared-scripts
RUN ${STAGE_DIR}/shared-scripts/bash/setupYarn.sh

# RUN bash/setupNVM.sh

############# IMPORTANT! Do not move the following code ################
WORKDIR ${HOME}
########################################################################

RUN echo "Home directory: ${HOME}"
RUN ls -halt ${HOME}

########### IMPORTANT! NVM must be setup after $HOME is coerced #######
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash

# Validate installation
RUN tree -L 2 $HOME

# # provision home directory & profile
# COPY scripts/.profile $HOME/.profile
# RUN . $HOME/.profile
RUN . $HOME/.bashrc
RUN . ${NVM_DIR}/nvm.sh

RUN echo "NPM: $(npm --version)"
RUN echo "Node: $(node --version)"
RUN echo "Yarn: $(yarn --version)"

# Finalize runtime context (for CI tasks)
WORKDIR ${STAGE_DIR}

# ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]